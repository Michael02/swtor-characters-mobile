export const iconConfig = require('./iconConfig.json');

export const ICONS = {
  swtor: 'swtor_icon',
  old_republic: 'old_republic',
  sith_empire: 'sith_empire',

  arrow_up: 'arrow_up_circle',
  arrow_left: 'arrow_left',

  add: 'add',
  minus: 'minus',
  photo_add: 'add_a_photo',

  locked: 'locked',
  search: 'search',
  settings: 'cogs',
  key: 'key',
  sort: 'sort_amount_asc',
  filter: 'filter',
  pencil: 'pencil',

  checked: 'check_box_checked',
  unchecked: 'check_box_unchecked',
  selected: 'selected',

  user_circle: 'user_circle',
  user_plus: 'user_plus',
  user: 'user',
  users: 'group',

  warning: 'warning',
  error: 'error',
  disconnected: 'disconnected',

  backspace: 'backspace',
  at_sign: 'at_sign',
  images: 'images',
};
