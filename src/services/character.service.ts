import FirebaseService from './firebase.service';

import { CharacterState } from 'store/characters/reducer';
import { keys } from 'helpers/array';
import * as storage from 'helpers/storage';
import { CHARACTER_OPTIONS } from 'constants/keys';

class CharacterService {
  addCharacter = async(data: CharacterState, uid: string) => {
    try {
      await FirebaseService.fireDB.ref(`users/${ uid }/characters/${ data.cid }`).set(data);
      return true;
    } catch (e) {
      return false;
    }
  }

  fetchUserCharacters = async(uid: string) => {
    const result: CharacterState[] = [];
    await FirebaseService.fireDB.ref(`users/${ uid }/characters/`).once('value', (snapshot) => {
      const snap = snapshot.val();
      keys(snap).map((key: string) => result.push(snap[key]));
    });
    return result;
  }

  getFactionsAndClasses = async() => {
    let result = null;
    await FirebaseService.fireDB.ref('factions/').once('value', (snapshot) => {
      const factions = snapshot.val();
      for (const faction in factions) {
        factions[faction] = keys(factions[faction]);
      }
      result = factions;
    });
    return result;
  }

  getServers = async() => {
    let result = null;
    await FirebaseService.fireDB.ref('servers/').once('value', (snapshot) => {
      const value = snapshot.val();
      const names = keys(value);
      const servers: any = [];
      names.map((name) => {
        servers.push({
          name,
          location: value[name].location,
        });
      });
      result = servers;
    });
    return result;
  }

  getSkills = async() => {
    let result = null;
    await FirebaseService.fireDB.ref('skills/').once('value', (snapshot) => {
      const skills = snapshot.val();
      for (const cat in skills) {
        skills[cat] = keys(skills[cat]);
      }
      result = skills;
    });
    return result;
  }

  getCharacterOptions = async() => {
    const servers = await this.getServers();
    const factions = await this.getFactionsAndClasses();
    const skills = await this.getSkills();
    const options = { factions, servers, skills };
    return options;
  }

  saveCharacterOptions = (options: { factions: any, servers: any, skills: any }) => {
    storage.save(CHARACTER_OPTIONS, options);
  }

  loadCharacterOptions = async() => {
    const result = await storage.load(CHARACTER_OPTIONS);
    return result;
  }
}

export default new CharacterService();
