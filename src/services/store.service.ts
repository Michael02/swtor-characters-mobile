import { Action } from 'redux';
import { Store } from 'store';

class StoreService {
  store: Store | null = null;

  setStore(store: Store) {
    this.store = store;
  }

  getState() {
    if (!this.store) return null;

    return this.store.getState();
  }

  dispatch(action: Action) {
    if (!this.store) return null;

    return this.store.dispatch(action);
  }
}

export default new StoreService();
