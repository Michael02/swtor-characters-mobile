import { NavigationActions, StackActions, NavigationContainerComponent } from 'react-navigation';

let _resolve: any;

class NavigationServiceClass {
  loaded: Promise<void>;
  navigator: NavigationContainerComponent | null = null;

  constructor() {
    this.loaded = new Promise((resolve) => {
      _resolve = resolve;
    });
  }

  setNavigator(navigator: any) {
    this.navigator = navigator;
    _resolve();
  }

  async navigate(routeName: string, params: Object = {}) {
    // await this.loaded;
    if (!this.navigator) return;

    this.navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      }),
    );
  }

  async replace(routeName: string, params: Object = {}) {
    // await this.loaded;
    if (!this.navigator) return;

    this.navigator.dispatch(
      // @ts-ignore
      StackActions.replace({
        key: '',
        routeName,
        params,
      }),
    );
  }

  goBack() {
    if (!this.navigator) return;

    this.navigator.dispatch(NavigationActions.back({ key: null }));
  }

  reset(routeName: string): void {
    if (!this.navigator) return;

    const reset = StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName })],
    });
    this.navigator.dispatch(reset);
  }
}

const NavigationService = new NavigationServiceClass();

export default NavigationService;
