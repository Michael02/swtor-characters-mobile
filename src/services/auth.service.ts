import FirebaseServece from './firebase.service';
import StoreService from './store.service';
import { signOut, userSet } from 'store/user';
import NavigationService from './navigation.service';
import { MAIN } from 'constants/routes';
import * as crypto from 'helpers/crypto';
import * as storage from 'helpers/storage';
import { USER_PROFILE } from 'constants/keys';

class AuthService {
  checkAuth = () => {
    FirebaseServece.auth.onAuthStateChanged((user) => {
      if (user) {
        const { email, uid, displayName } = user;
        StoreService.dispatch(userSet(email, uid, displayName));
      } else this.signout();
    });
  }

  login = async(email: string, password: string) => {
    let result = false;
    await FirebaseServece.auth.signInWithEmailAndPassword(email, password)
      .then((response) => {
        if (response.user) {
          const { uid, displayName = '' } = response.user;
          StoreService.dispatch(userSet(email, uid, displayName));
          this.saveUser(uid, email, password, displayName);
          NavigationService.navigate(MAIN);
          result = true;
        }
      })
      .catch((error) => console.log('fire login', error));
    return result;
  }

  signup = async(email: string, password: string, displayName: string = '') => {
    FirebaseServece.auth.createUserWithEmailAndPassword(email, password)
      .then((response) => {
        if (response.user) {
          const { uid } = response.user;
          FirebaseServece.fireDB.ref(`users/${ uid }/`).set({ email });
          StoreService.dispatch(userSet(email, uid, displayName));
          this.saveUser(uid, email, password, displayName);
          if (displayName) {
            const user = FirebaseServece.auth.currentUser;
            if (user) user.updateProfile({ displayName, photoURL: null });
          }
          NavigationService.navigate(MAIN);
        }
      })
      .catch((error) => console.log('fire signup', error));
  }

  saveUser = (uid: string, email: string, password: string, displayName: string | null = '') => {
    const hashPassword = crypto.encrypt(password);
    storage.save(USER_PROFILE, {
      uid,
      email,
      password: hashPassword,
      displayName,
    });
  }

  forgotPassword = (email: string) => {
    FirebaseServece.auth.sendPasswordResetEmail(email);
  }

  signout = async() => {
    await FirebaseServece.auth.signOut();
    StoreService.dispatch(signOut());
  }
}

export default new AuthService();
