import { NetInfo } from 'react-native';
import StoreService  from './store.service';
import { CONNECTION_CHANGE } from 'store/global/constants';
import { create } from 'helpers/utils';

class NetworkService {
  constructor() {
    NetInfo.isConnected.addEventListener('connectionChange', this.handleChange);
  }

  init = () => {
    NetInfo.isConnected.fetch()
      .then((isConnected) => {
        this.handleChange(isConnected);
      })
      .catch((error) => console.log('isConnected error: ', error));
  }

  handleChange = (isOnline: boolean) => {
    StoreService.dispatch(create(CONNECTION_CHANGE, isOnline));
  }
}

export default new NetworkService();
