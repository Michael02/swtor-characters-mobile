const RNFetchBlob = require('rn-fetch-blob');
import * as storage from 'helpers/storage';

class ImageService {
  fetchImage = async(url: string) => {
    const value = await storage.load(url);
    if (value) return;
    RNFetchBlob.fetch('GET', url)
      .then((res: any) => {
        const info = res.info();
        if (info.status === 200) {
          const base64 = res.base64();
          const type = info.headers['Content-Type'];  // image/png
          storage.save(url, `data:${type};base64,${base64}`);
        }
      })
      .catch((error: any) => console.log(error));
  }

  getImage = async(url: string) => {
    const value = await storage.load(url);
    return value;
  }

  removeImage = (url: string) => {
    storage.remove(url);
  }
}

export default new ImageService();
