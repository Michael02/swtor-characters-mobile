export const MIN_PASSWORD_LEN = 4;

export const MAX_CHARACTER_LVL = 70;
export const MAX_COMMANDER_LVL = 300;
export const MAX_SKILL_LVL = 600;
