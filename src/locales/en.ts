export default {
  sw: 'Star Wars',
  tor: 'The Old Republic',
  app_name: 'SWTOR Characters',
  login: {
    email: 'Enter your email',
    password: 'Enter your password',
  },
  signup: {
    display_name: 'Enter display name (not required)',
    email: 'Enter your email',
    password: 'Enter your password',
    password_confirm: 'Confirm your password',
  },
  characters_list: {
    add_character: 'Add a new character',
  },
  character_add: {
    server: 'Choose Server:',
    faction: 'Choose Faction:',
    class_name: 'Choose Class:',
    name: 'Enter Name:',
    level: 'Enter Level:',
    commander_level: 'Enter Comander Lvl.:',
    crew_skills: 'Select Crew Skills:',
    images: 'Add Images:',
    button: 'Add Character',
  },
  label: {
    gallery: 'Gallery',
    characters: 'Character',
    settings: 'Settings',
    characters_list: 'Characters List',
    character_add: 'Add Character',
    character_edit: 'Character',
  },
  button: {
    login: 'Login',
    signup: 'Signup',
    close: 'Close',
    choose: 'Choose',
  },
};
