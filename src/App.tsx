import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { RootStack } from './Navigation';

import store from 'store';
import StoreService from 'services/store.service';
import NavigationService from 'services/navigation.service';
import NetworkService from 'services/network.service';

StoreService.setStore(store);

type Props = {};

export default class App extends Component<Props> {
  componentDidMount() {
    NetworkService.init();
    console.disableYellowBox = true;
  }

  render() {
    return (
      <Provider store={ store }>
        <React.Fragment>
          <RootStack
            ref={ (navigator: any) => NavigationService.setNavigator(navigator) }
          />
        </React.Fragment>
      </Provider>
    );
  }
}
