import Storage from 'react-native-storage';
import { AsyncStorage } from 'react-native';

const storage = new Storage({
  size: 1000,
  storageBackend: AsyncStorage,
  defaultExpires: null,
  enableCache: true,
  sync : {},
});

type Options = {
  expires?: number,
};

export const save = async(key: string, dataToSave: any, options: Options = {}) => {
  const data = JSON.stringify(dataToSave);
  storage.save({ key, data, ...options });
};

export const load = async(key: string) => {
  const defaultOptions = {
    autoSync: false,
    syncInBackground: false,
  };
  try {
    const result = await storage.load({
      key,
      ...defaultOptions,
    });
    if (result) return JSON.parse(result);
    throw new Error();
  } catch {
    return null;
  }
};

export const clear = () => {
  AsyncStorage.clear();
};

export const remove = (key: string) => storage.remove({ key });
