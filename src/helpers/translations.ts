const data = require('locales/en').default;

export const get = (key: string): string => {
  if (!key) {
    throw new Error('key is required!');
  }

  if (data === null) {
    return '';
  }

  const keys = key.split('.');
  let msg = data;
  for (const k in keys) {
    if (!msg[keys[k]]) {
      msg = null;
      break;
    }
    msg = msg[keys[k]];
  }

  if (msg === null) {
    console.warn(`[translations] key '${ key }' not found!`);
    return '';
  }

  if (typeof msg !== 'string') {
    console.warn(`[translations] value of key '${ key }' is not a string!`);
    return '';
  }

  return msg;
};
