export const newNumArray = (length: number): number[] => Array.apply(null, { length }).map(Number.call, Number); // create array [0 - length-1]
export const removeIndex = (array: any[], index: number) => {
  array.splice(index, 1);
  return array;
};
export const keys = (dict: {}) => Object.keys(dict);
