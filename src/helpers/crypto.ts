import CryptoJS from 'crypto-js';
import { SECRET_KEY } from '../../.env';

export const encrypt = (text: string) => {
  if (!SECRET_KEY) throw new Error(`You don't have a KEY`);
  return CryptoJS.AES.encrypt(text, SECRET_KEY).toString();
};

export const decrypt = (cipherText: string) => {
  if (!SECRET_KEY) throw new Error(`You don't have a KEY`);
  return CryptoJS.AES.decrypt(cipherText, SECRET_KEY).toString(CryptoJS.enc.Utf8);
};

export const md5 = (text: string) =>
  CryptoJS.MD5(text).toString();
