export const upper = (text: string) => text.toUpperCase();
export const lower = (text: string) => text.toLowerCase();
export const capitalize = (text: string) => upper(text.charAt(0)) + text.slice(1);
