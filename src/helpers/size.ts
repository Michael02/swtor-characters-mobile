import { Dimensions } from 'react-native';

export const getWindowSize = () => Dimensions.get('window');
export const getScreenSize = () => Dimensions.get('screen');

export const getWindowWidth = () => getWindowSize().width;
export const getWindowHeight = () => getWindowSize().height;
