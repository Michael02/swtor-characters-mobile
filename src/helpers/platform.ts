import { Platform } from 'react-native';

export const platform = Platform.OS;
export const isIOS = () => platform === 'ios';
