export const delay = (duration: number = 250) => new Promise((resolve) => window.setTimeout(resolve,  duration));
export const create = (type: string, payload: any = {}) => ({ type, payload });
