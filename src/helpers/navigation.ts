export const getCurrentRouteName = (state: any) => {
  const { routes = null, index = 0 } = state;
  return (routes && routes.length) ? routes[index].routeName : '';
};
