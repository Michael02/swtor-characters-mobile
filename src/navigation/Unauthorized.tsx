import { createStackNavigator } from 'react-navigation';
import * as ROUTES from 'constants/routes';
import Login from 'pages/Login';
import Signup from 'pages/Signup';
import Disconnected from 'pages/Disconnected';

export const unauthorizedNavigator = createStackNavigator(
  {
    [ROUTES.LOGIN]: {
      screen: Login,
      navigationOptions: {
        header: null,
      },
    },
    [ROUTES.SIGNUP]: {
      screen: Signup,
      navigationOptions: {
        header: null,
      },
    },
    [ROUTES.DISCONNECTED]: {
      screen: Disconnected,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: ROUTES.LOGIN,
  },
);
