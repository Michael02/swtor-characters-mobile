import React from 'react';
import { createBottomTabNavigator, NavigationScreenConfig } from 'react-navigation';
import * as ROUTES from 'constants/routes';

import CharactersScreen from 'pages/Characters';
import Settings from 'pages/Settings';
import Gallery from 'pages/Gallery';
import { BG_TABBAR, GOLD_DARK, GOLD } from 'styles/global';
import Icon from 'components/Icon';
import { get } from 'helpers/translations';
import { getCurrentRouteName } from 'helpers/navigation';

const tabBarVisible = [
  ROUTES.CHARACTERS_LIST,
  ROUTES.GALLERY,
  ROUTES.SETTINGS,
  ROUTES.SETTINGS_HOME,
];

const renderIcon = (route: string, focused: boolean) => {
  const images = {
    [ROUTES.GALLERY]: 'images',
    [ROUTES.CHARACTERS]: 'users',
    [ROUTES.SETTINGS]: 'settings',
  };

  return (
    <Icon
      name={ images[route] }
      size={ 26 }
      color={ focused ? GOLD : GOLD_DARK }
    />
  );
};

export const MainNavigation = createBottomTabNavigator(
  {
    [ROUTES.GALLERY]: {
      screen: Gallery,
      navigationOptions: {
        title: `${ get('label.gallery') }`,
        tabBarIcon: ({ focused }: any) => renderIcon(ROUTES.GALLERY, focused),
      },
    },
    [ROUTES.CHARACTERS]: {
      screen: CharactersScreen,
      navigationOptions: ({ navigation }: NavigationScreenConfig<any>) => {
        const currentRoute = getCurrentRouteName(navigation.state);
        return {
          title: `${ get('label.characters') }`,
          tabBarIcon: ({ focused }: any) => renderIcon(ROUTES.CHARACTERS, focused),
          tabBarVisible: currentRoute === '' || tabBarVisible.indexOf(currentRoute) !== -1,
        };
      },
    },
    [ROUTES.SETTINGS]: {
      screen: Settings,
      navigationOptions: ({ navigation }: NavigationScreenConfig<any>) => {
        const currentRoute = getCurrentRouteName(navigation.state);
        return {
          title: `${ get('label.settings') }`,
          tabBarIcon: ({ focused }: any) => renderIcon(ROUTES.SETTINGS, focused),
          tabBarVisible: currentRoute === '' || tabBarVisible.indexOf(currentRoute) !== -1,
        };
      },
    },
  },
  {
    initialRouteName: ROUTES.CHARACTERS,
    tabBarOptions: {
      activeTintColor: GOLD,
      inactiveTintColor: GOLD_DARK,
      labelStyle: {},
      showLabel: false,
      scrollEnabled: false,
      style: {
        height: 49,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: BG_TABBAR,
        borderTopColor: GOLD_DARK,
        borderTopWidth: 1,
      },
    },
  },
);
