import { IconName } from './icon';

export type ListItem = {
  label: string,
  info?: string,
  icon?: {
    name: IconName,
    color: string,
  },
  selected?: boolean,
  checked?: boolean,
};
