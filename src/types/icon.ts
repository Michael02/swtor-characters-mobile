import { ICONS } from 'assets/icons';

export type IconName = keyof typeof ICONS;
