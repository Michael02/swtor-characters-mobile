export type AddItem = {
  type: 'input' | 'button' | 'number',
  label: string,
  property: 'server' | 'faction' | 'name' | 'lvl' | 'crewSkills' | 'className' | 'commanderLvl',
  render: boolean,

  onPress?: (t?: any) => void,

  value?: string,
  onChange?: (e: string) => void,
};
