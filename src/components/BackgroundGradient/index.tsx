import React from 'react';
import { ScrollView, ScrollViewProps, View } from 'react-native';
import LinearGradient, { LinearGradientProps } from 'react-native-linear-gradient';
import { BG_GRADIENT_BOTTOM, BG_GRADIENT_TOP } from 'styles/global';
import { getWindowWidth } from 'helpers/size';

type PropsKeys = Exclude<keyof LinearGradientProps, 'colors'>;
type Props = { fixed?: boolean } & Partial<{ [K in PropsKeys]: LinearGradientProps[K]; } & ScrollViewProps>;

const BackgroundGradient: React.SFC<Props> = ({ children, fixed = false, ...props }) => {
  if (fixed) {
    return (
      <View style={ { flex: 1 } }>
        <LinearGradient colors={ [BG_GRADIENT_TOP, BG_GRADIENT_BOTTOM] } { ...props }>
            { children }
        </LinearGradient>
      </View>
    );
  }
  return (
    <ScrollView contentContainerStyle={ { height: '100%', width: getWindowWidth() } } keyboardShouldPersistTaps="handled">
      <LinearGradient colors={ [BG_GRADIENT_TOP, BG_GRADIENT_BOTTOM] } { ...props }>
          { children }
      </LinearGradient>
    </ScrollView>
  );
};

export default BackgroundGradient;
