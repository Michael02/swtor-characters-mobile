import React from 'react';
import { View, ViewStyle, StyleSheet } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import { iconConfig, ICONS } from 'assets/icons';
import { IconName } from 'types/icon';

const CustomIcon = createIconSetFromIcoMoon(iconConfig);

type Props = {
  name: IconName,
  size?: number,
  color?: string,
  style?: ViewStyle,
};

const Icon: React.SFC<Props> = ({ name = '', color = '#000', size = 24, style = {} }) => {
  return (
    <View style={ [styles.imageWrapper, style] }>
      <CustomIcon
        name={ ICONS[name] }
        color={ color }
        size={ size }
      />
    </View>
  );
};

type Styles = {
  imageWrapper: ViewStyle,
};

const styles = StyleSheet.create<Styles>({
  imageWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
  },
});

export default Icon;
