import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import Icon from 'components/Icon';
import { IconName } from 'types/icon';

type Props = {
  label?: string,
  icon?: IconName | null,
  onPress?: (e: string) => void,
};

const Key: React.SFC<Props> = ({ label = '', icon = null, onPress = () => null }) => {
  return (
    <TouchableOpacity
      onPress={ () => label ? onPress(label) : null }
      style={ styles.keyContainer }
    >
      <View style={ [styles.keyStyle, label && styles.keyWithLabel] }>
        {
          icon ?
          <Icon
            name={ icon }
          /> :
          <Text style={ styles.keyText }>{ label }</Text>
        }
      </View>
    </TouchableOpacity>
  );
};

type Styles = {
  keyContainer: ViewStyle,
  keyStyle: ViewStyle,
  keyWithLabel: ViewStyle,
  keyText: TextStyle,
};

const styles = StyleSheet.create<Styles>({
  keyContainer: {},
  keyStyle: {},
  keyWithLabel: {},
  keyText: {},
});

export default Key;
