import React from 'react';
import { View, ViewStyle, StyleSheet } from 'react-native';
import Key from './Key';
import { newNumArray } from 'helpers/array';
import { getWindowWidth } from 'helpers/size';

type Props = {
  onPress: (key: string) => void,
  onBackspace?: () => void,
  show?: boolean,
  biometricsLogin?: () => void,
  biometrics?: 'faceId' | 'touchId' | null,
  showSeparator?: boolean,
};


class NumberKeyboard extends React.Component<Props> {
  renderNumberKeys = () => {
    const { onPress = () => null } = this.props;
    const numArray = newNumArray(9);
    return numArray.map((num: number) =>
      <Key label={ `${num + 1}` } onPress={ (key: string) => onPress(key) } key={ num }/>);
  }

  render() {
    const {
      onBackspace = () => null,
      show = true,
      biometricsLogin = () => null,
      biometrics = null,
      showSeparator = false,
    } = this.props;
    if (!show) return null;
    return (
      <View style={ styles.keyboard }>
        { this.renderNumberKeys() }
        {
          // biometricsLogin && biometrics === 'faceId' ?
          // <Key icon="faceID" onPress={ () => biometricsLogin() } /> :
          // biometricsLogin && biometrics === 'touchId' ?
          // <Key icon="touchID" onPress={ () => biometricsLogin() } /> :
          showSeparator ?
          <Key label="." onPress={ () => this.props.onPress('.') } /> :
          <Key/>
        }
        <Key label="0" onPress={ () => this.props.onPress('0') } />
        { this.props.onBackspace && <Key icon="backspace" onPress={ () => onBackspace() } /> }
      </View>
    );
  }
}

type Styles = {
  keyboard: ViewStyle,
};

const styles = StyleSheet.create<Styles>({
  keyboard: {
    width: getWindowWidth(),
    padding: 6,
    paddingRight: 0,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});

export default NumberKeyboard;
