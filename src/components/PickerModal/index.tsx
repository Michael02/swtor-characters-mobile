import React from 'react';
import { Modal, Animated, FlatList, Text, StyleSheet, ViewStyle, TouchableOpacity, TextStyle, View } from 'react-native';
import { getWindowWidth, getWindowHeight } from 'helpers/size';
import { WHITE, GREEN, GRAY_LIGHT } from 'styles/global';
import { ListItem } from 'types/listItem';
import Icon from 'components/Icon';
import { get } from 'helpers/translations';
import Button from 'components/Button';

type Props = {
  visible: boolean,
  data: ListItem[],
  onSelect: (e: string) => void,
  onClose?: (() => void) | null,
};
type State = {
  showModal: boolean,
};

class PickerModal extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      showModal: false,
    };
  }
  backOpacityAnim = new Animated.Value(0);
  mainOpacityAnim = new Animated.Value(0);
  duration = 400;

  componentDidUpdate(prevProps: Props) {
    console.log('picker modal componentDidUpdate: ', this.props);
    const { visible } = this.props;
    if (visible && !prevProps.visible) this.onShow();
    else if (!visible && prevProps.visible) this.onHide();
  }

  onShow = () => {
    this.setState({ showModal: true });
    Animated.parallel([
      Animated.timing(this.backOpacityAnim, {
        toValue: .4,
        duration: this.duration,
      }),
      Animated.timing(this.mainOpacityAnim, {
        toValue: 1,
        duration: this.duration,
      }),
    ]).start();
  }

  onHide = () => {
    Animated.parallel([
      Animated.timing(this.backOpacityAnim, {
        toValue: 0,
        duration: this.duration,
      }),
      Animated.timing(this.mainOpacityAnim, {
        toValue: 0,
        duration: this.duration,
      }),
    ]).start();
    setTimeout(() => this.setState({ showModal: false }), this.duration * 1.5);
  }

  renderItem = (item: ListItem) => {
    return (
      <TouchableOpacity
        onPress={ () => this.props.onSelect(item.label) }
        style={ styles.itemWrapper }
      >
        {
          item.icon &&
          <Icon
            name={ item.icon.name }
            size={ 30 }
            style={ styles.itemIcon }
            color={ item.icon.color }
          />
        }
        {
          'checked' in item &&
          <Icon
            name={ item.checked ? 'checked' : 'unchecked' }
            size={ 30 }
            style={ styles.itemIcon }
            color={ item.checked ? GREEN : GRAY_LIGHT }
          />
        }
        <Text style={ styles.itemLabel }>{ item.label }</Text>
        { item.info && <Text>{ item.info }</Text> }
        {
          item.selected ?
          <Icon
            name="selected"
            size={ 40 }
            style={ styles.itemSelected }
          /> :
          <View style={ styles.itemSelected }/>
        }
      </TouchableOpacity>
    );
  }

  renderSeparator = () => <View style={ styles.separatorWrapper }><View style={ styles.separator }/></View>;

  render() {
    const { data, onClose = null } = this.props;
    return (
      <Modal
        visible={ this.state.showModal }
        transparent={ true }
        animationType="none"
        onRequestClose={ () => null }
      >
        <View style={ styles.contentWrapper }>
          <Animated.View style={ [styles.background, { opacity: this.backOpacityAnim }] }/>
          <Animated.View style={ [styles.pickerBody, { opacity: this.mainOpacityAnim }] }>
            <FlatList
              data={ data }
              keyExtractor={ (item) => item.label }
              renderItem={ ({ item }) => this.renderItem(item) }
              ItemSeparatorComponent={ () => this.renderSeparator() }
            />
            {
              onClose &&
              <Button
                text={ get('button.close') }
                onPress={ () => onClose() }
                styleButton={ styles.closeButton }
                gradient={ false }
              />
            }
          </Animated.View>
        </View>
      </Modal>
    );
  }
}

type Styles = {
  background: ViewStyle,
  contentWrapper: ViewStyle,
  pickerBody: ViewStyle,

  itemWrapper: ViewStyle,
  itemIcon: ViewStyle,
  itemLabel: TextStyle,
  itemInfo: TextStyle,
  itemSelected: ViewStyle,

  separatorWrapper: ViewStyle,
  separator: ViewStyle,

  closeButton: ViewStyle,
};

const textStyle: TextStyle = {
  fontSize: 18,
  lineHeight: 20,
};

const styles = StyleSheet.create<Styles>({
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
    zIndex: -1,
    backgroundColor: '#000',
  },
  contentWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pickerBody: {
    width: getWindowWidth() - 60,
    maxHeight: getWindowHeight() - 200,
    backgroundColor: WHITE,
  },
  itemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    height: 45,
    width: '100%',
  },
  itemIcon: {
    width: 35,
    height: 35,
    marginRight: 10,
  },
  itemLabel: {
    flex: 1,
    ...textStyle,
  },
  itemInfo: {
    ...textStyle,
    textAlign: 'right',
  },
  itemSelected: {
    width: 35,
    height: 35,
  },

  separatorWrapper: {
    paddingHorizontal: 10,
  },
  separator: {
    borderTopColor: '#000',
    borderTopWidth: 1,
  },

  closeButton: {
    height: 35,
    backgroundColor: GRAY_LIGHT,
  },
});

export default PickerModal;
