import React from 'react';
import { View, TextInput, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { IconName } from 'types/icon';
import { GOLD_LIGHT, GOLD, GOLD_DARK, GRAY_LIGHT, GRAY_DARK } from 'styles/global';
import Icon from 'components/Icon';

type Props = {
  config : {
    value: string,
    placeholder: string,
    icon: IconName,
    onChange: (e: string) => void,
    inputProps: {},
    ref?: any,
  },
};

const LoginInput: React.SFC<Props> = ({ config }) => {
  const {
    value = '',
    placeholder = '',
    icon = '',
    onChange = () => null,
    inputProps = {},
    ref = null,
  } = config;
  return (
    <View style={ styles.inputWrapper }>
      <Icon
        name={ icon }
        style={ styles.icon }
        size={ 26 }
        color={ GOLD }
      />
      <TextInput
        ref={ ref ? ref : null }
        value={ value }
        onChangeText={ (text) => onChange(text) }
        placeholder={ placeholder }
        style={ styles.input }
        underlineColorAndroid="transparent"
        placeholderTextColor={ GOLD_LIGHT }
        selectionColor={ GRAY_LIGHT }
        autoCapitalize="none"
        autoCorrect={ false }
        blurOnSubmit={ true }
        { ...inputProps }
      />
    </View>
  );
};

type Styles = {
  inputWrapper: ViewStyle,
  icon: ViewStyle,
  input: TextStyle,
};

const styles = StyleSheet.create<Styles>({
  inputWrapper: {
    flexDirection: 'row',
    borderColor: GOLD_DARK,
    borderWidth: 3,
    borderRadius: 5,
    height: 45,
    backgroundColor: GRAY_DARK,
    alignItems: 'center',
    marginBottom: 15,
    paddingHorizontal: 10,
  },
  icon: {
    marginRight: 10,
  },
  input: {
    flex: 1,
    fontSize: 18,
    lineHeight: 22,
    color: GOLD,
    height: 40,
    paddingVertical: 5,
  },
});

export default LoginInput;
