import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ViewStyle, TextStyle } from 'react-native';
import { CharacterState } from 'store/characters/reducer';
import Icon from 'components/Icon';
import CharacterImage from 'components/CharacterImage';
import { EMP_RED, REP_BLUE, GOLD_LIGHT } from 'styles/global';

type Props = {
  character: CharacterState,
  onPress: (c: CharacterState) => void,
  last: boolean,
};

const CharacterItem: React.SFC<Props> = ({ character, onPress, last }) => {
  const {
    name,
    level,
    server,
    faction,
    className,
    images = [],
  } = character;

  const textTop = `${ name }, lvl. ${ level }, ${ className }`;
  const textBottom = `${ server }`;
  return (
    <TouchableOpacity
      onPress={ () => onPress(character) }
      style={ [styles.container, last && { marginBottom: 20 }] }
    >
      {
        images.length ?
        <CharacterImage url={ images[0] } style={ styles.image }/> : null
      }
      <View style={ styles.infowrapper }>
        <Text style={ styles.infoText }>{ textTop }</Text>
        <Text style={ styles.infoText }>{ textBottom }</Text>
      </View>
      <Icon
        name={ faction === 'Sith Empire' ? 'sith_empire' : 'old_republic' }
        style={ styles.factionIcon }
        color={ faction === 'Sith Empire' ? EMP_RED : REP_BLUE }
        size={ 30 }
      />
    </TouchableOpacity>
  );
};

type Styles = {
  container: ViewStyle,
  image: ViewStyle,
  infowrapper: ViewStyle,
  infoText: TextStyle,
  factionIcon: ViewStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 5,
    height: 70,
    width: '100%',
  },
  image: {
    marginRight: 10,
    width: 60,
    height: 60,
    borderRadius: 100,
  },
  infowrapper: {
    flex: 1,
    justifyContent: 'space-around',
  },
  infoText: {
    color: GOLD_LIGHT,
  },
  factionIcon: {
    marginLeft: 10,
    width: 60,
    height: 60,
  },
});

export default CharacterItem;
