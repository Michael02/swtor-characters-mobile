import React from 'react';
import { TouchableOpacity, Text, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { GRAY_LIGHT, BTN_TOP, BTN_BOTTOM, GRAY } from 'styles/global';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'components/Spinner';

type Props = {
  text: string,
  onPress: () => void,
  styleButton?: ViewStyle,
  styleText?: TextStyle,
  disabled?: boolean,
  gradient?: boolean,
  colors?: string[],
  locations?: number[],
  loading?: boolean,
};

const Button: React.SFC<Props> = ({
  text = '',
  onPress = () => null,
  styleButton = {},
  styleText = {},
  disabled = false,
  gradient = true,
  colors = [BTN_TOP, BTN_BOTTOM],
  locations = [0.45, 0.65],
  loading = false,
}) => {
  const colorsDisabled = [GRAY_LIGHT, GRAY];

  const content = loading ? <Spinner type="ball" size={ 24 } color="#da0"/> : <Text style={ [styles.text, styleText] }>{ text }</Text>;

  return (
    <TouchableOpacity
      onPress={ onPress }
      disabled={ disabled }
      accessibilityComponentType="button"
      style={ [styles.button, styleButton, disabled && styles.disabled, !gradient && styles.center] }
    >
      {
        gradient ?
        <LinearGradient
          colors={ disabled ? colorsDisabled : colors }
          style={ [styles.backGround, styles.center] }
          locations={ locations }
        >
          { content }
        </LinearGradient> : content
      }
    </TouchableOpacity>
  );
};

type Styles = {
  button: ViewStyle,
  backGround: ViewStyle,
  text: TextStyle,
  disabled: ViewStyle,
  center: ViewStyle,
};

const styles = StyleSheet.create<Styles>({
  button: {
    height: 40,
    width: '100%',
  },
  backGround: {
    flex: 1,
  },
  text: {
    color: '#fff',
  },
  disabled: {
    backgroundColor: GRAY_LIGHT,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Button;
