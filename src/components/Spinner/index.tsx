import React from 'react';
import {
  BallIndicator,
  SkypeIndicator,
  MaterialIndicator,
} from 'react-native-indicators';

type Props = {
  size?: number,
  color?: string,
  type?: 'ball' | 'skype' | 'material',
};

const Spinner: React.SFC<Props> = ({ size = 40, color = '#000', type = 'material' }) => {
  switch (type) {
    case 'ball': {
      return <BallIndicator color={ color } size={ size }/>;
    }
    case 'skype': {
      return <SkypeIndicator color={ color } size={ size }/>;
    }
    case 'material': {
      return <MaterialIndicator color={ color } size={ size }/>;
    }
    default: {
      return null;
    }
  }
};

export default Spinner;
