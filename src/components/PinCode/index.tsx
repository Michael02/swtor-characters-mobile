import React from 'react';
import { StyleSheet, Animated, View, Text, ViewStyle } from 'react-native';
import BackgroundGradient from 'components/BackgroundGradient';
import NumberKeyboard from 'components/NumberKeyboard';
import { newNumArray } from 'helpers/array';

type Props = {
  pin: string,
  onChange: (e: string) => void,
  onBackspace: () => void,
  title: string,
  subtitle?: string,
  animate?: boolean,
  animationDuration?: number,
};
type State = {};

export default class PinCode extends React.Component<Props, State> {
  deltaXY = new Animated.ValueXY();

  animatePin = () => {
    const { animationDuration = 80 } = this.props;
    Animated.sequence([
      Animated.timing(this.deltaXY, {
        toValue: { x: -20, y: 0 },
        duration: animationDuration,
      }),
      Animated.timing(this.deltaXY, {
        toValue: { x: 20, y: 0 },
        duration: animationDuration * 1.5,
      }),
      Animated.timing(this.deltaXY, {
        toValue: { x: 0, y: 0 },
        duration: animationDuration,
      }),
    ]).start();
  }

  renderPin = () => {
    const { pin = '' } = this.props;
    const indicatorArray = newNumArray(4);
    return (
      <Animated.View style={ [styles.pinCode, { transform: this.deltaXY.getTranslateTransform() }] }>
        {
          indicatorArray.map((item: number) => (
            <View>
              { item < pin.length && <View/> }
            </View>
          ))
        }
      </Animated.View>
    );
  }

  render() {
    const {
      onChange,
      onBackspace,
      title = '',
      subtitle = '',
    } = this.props;
    return (
      <BackgroundGradient fixed={ true } style={ styles.container }>
        <View>
          <Text>{ title }</Text>
          { subtitle && <Text>{ subtitle }</Text> }
          { this.renderPin() }
        </View>
        <NumberKeyboard
          onPress={ (t: string) => onChange(t) }
          onBackspace={ () => onBackspace() }
        />
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,

  pinCode: ViewStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
  },

  pinCode: {
    flexDirection: 'row',
  },
});
