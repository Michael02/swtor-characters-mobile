import React from 'react';
import { View, Image, StyleSheet, ViewStyle, ImageStyle } from 'react-native';
import ImageService from 'services/image.service';

type Props = {
  url: string,
  style?: ViewStyle,
};
type State = {
  imageData: string,
};

class CharacterImage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      imageData: '',
    };
  }

  componentWillMount = () => {
    if (!this.props.url) return;
    this.setImageData();
  }

  componentDidUpdate = (prevProps: Props) => {
    if (!this.props.url || prevProps.url === this.props.url) return;
    this.setImageData();
  }

  setImageData = async() => {
    const imageData = await ImageService.getImage(this.props.url);
    if (imageData) this.setState({ imageData });
    else this.setState({ imageData: '' });
  }

  render() {
    const { url = '', style = {} } = this.props;
    const { imageData } = this.state;
    const image = imageData ? imageData : url;

    return (
      <View style={ [styles.container, style] }>
        <Image
          source={ { uri: image } }
          style={ styles.image }
        />
      </View>
    );
  }
}

type Styles = {
  container: ViewStyle,
  image: ImageStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    width: 150,
    height: 150,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
});

export default CharacterImage;
