export const BG_GRADIENT_TOP = '#1E1E1E';
export const BG_GRADIENT_BOTTOM = '#191919';

export const WHITE = '#FFF';

export const GOLD = '#E2D872';
export const GOLD_LIGHT = '#FFF8C6';
export const GOLD_DARK = '#B48932';

export const GRAY = '#888';
export const GRAY_LIGHT = '#CCC';
export const GRAY_DARK = '#292929';

export const BG_TABBAR = BG_GRADIENT_TOP;
export const BG_NAVBAR = GRAY_DARK;

export const BTN_TOP = '#CC9E42';
export const BTN_BOTTOM = '#AE8638';

export const REP_BLUE = '#006DE3';
export const EMP_RED = '#E60101';

export const GREEN = '#2EC92E';
