import { StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { BG_NAVBAR, GOLD, WHITE } from './global';

type Styles = {
  header: ViewStyle,
  headerLeft: ViewStyle,
  headerRight: ViewStyle,
  headerTitle: TextStyle,
};

export const styles = StyleSheet.create<Styles>({
  header: {
    backgroundColor: BG_NAVBAR,
    borderBottomColor: GOLD,
    borderBottomWidth: 1,
    shadowColor: 'transparent',
  },
  headerLeft: {
    width: 35,
    height: 35,
    marginLeft: 15,
    justifyContent: 'center',
    // borderColor: '#0f0',
    // borderWidth: 1,
  },
  headerRight: {
    width: 35,
    height: 35,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
    // borderColor: '#0f0',
    // borderWidth: 1,
  },
  headerTitle: {
    color: WHITE,
    flex: 1,
    marginHorizontal: 0,
    fontSize: 18,
    lineHeight: 20,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
  },
});
