import { createSwitchNavigator } from 'react-navigation';

import { MainNavigation } from 'navigation/Main';
import { unauthorizedNavigator } from 'navigation/Unauthorized';
import Splash from 'pages/Splash';
import * as ROUTES from 'constants/routes';

const INITIAL_ROUTER_NAME = ROUTES.SPLASH;

export const RootStack = createSwitchNavigator(
  {
    [ROUTES.SPLASH]: Splash,
    [ROUTES.MAIN]: MainNavigation,
    [ROUTES.UNAUTHORIZED]: unauthorizedNavigator,
  },
  {
    initialRouteName: INITIAL_ROUTER_NAME,
  },
);
