import { Action } from 'types/store';
import * as TYPES from './constants';

export type CharacterState = {
  cid: string,
  server: string,
  faction: string,
  className: string,
  name: string,
  level: number,
  crewSkills: string[],
  commanderLevel: number,
  images: string[],
  created: string,
  modified: string,
};

type FiltersState = {
  className: string,
  faction: string,
  server: string,
  crewSkills: string[],
  sortBy: string,
};

export type CharactersState = {
  list: CharacterState[],
  filters: FiltersState,
};

// tslint:disable:prefer-array-literal
export const charactersInitialState = {
  list: new Array<CharacterState>(),
  filters: {
    className: '',
    faction: '',
    server: '',
    crewSkills: new Array<string>(),
    sortBy: '',
  },
};

const updateCharacter = (characters: CharacterState[], data: Partial<CharacterState>) => {
  const character = characters.find((c) => c.cid === data.cid);
  if (character) {
    const index = characters.indexOf(character);
    for (const key in data) {
      character[key] = data[key];
    }
    characters[index] = character;
  }

  return characters;
};

const removeCharacter = (characters: CharacterState[], id: string) => {
  const data = characters.filter((c) => c.cid !== id);
  return data;
};

export const charactersReducer = (state = charactersInitialState, action: Action) => {
  const { type, payload } = action;
  switch (type) {
    case TYPES.CHARACTER_ADD: {
      state.list.push(payload);
      return { ...state };
    }
    case TYPES.CHARACTER_ADD_SET: {
      return {
        ...state,
        list: [...payload],
      };
    }
    case TYPES.CHARACTER_UPDATE: {
      return {
        ...state,
        list: updateCharacter(state.list, payload),
      };
    }
    case TYPES.CHARACTER_REMOVE: {
      return {
        ...state,
        list: removeCharacter(state.list, payload),
      };
    }
    default: {
      return { ...state };
    }
  }
};
