import Redux from 'redux';
import * as TYPES from './constants';
import { CharacterState } from './reducer';
import { create } from 'helpers/utils';
import StoreService from 'services/store.service';
import CharacterService from 'services/character.service';

export const characterAdd = (data: CharacterState) => async(dispatch: Redux.Dispatch<any>) => {
  const store = StoreService.getState();
  if (!store) return;
  const { uid } = store.user;
  if (!uid) return;
  const saved = await CharacterService.addCharacter(data, uid);
  if (!saved) return false;
  dispatch(create(TYPES.CHARACTER_ADD, data));
  return true;
};

export const fetchCharacters = () => async(dispatch: Redux.Dispatch<any>) => {
  const store = StoreService.getState();
  if (!store || !store.user || !store.user.uid) return;
  const { uid } = store.user;
  const characters = await CharacterService.fetchUserCharacters(uid);
  dispatch(create(TYPES.CHARACTER_ADD_SET, characters));
};

export const characterUpdate = (data: Partial<CharacterState>) => (dispatch: Redux.Dispatch<any>) =>
  dispatch(create(TYPES.CHARACTER_UPDATE, data));

export const characterRemove = (data: string) => (dispatch: Redux.Dispatch<any>) =>
  dispatch(create(TYPES.CHARACTER_REMOVE, data));
