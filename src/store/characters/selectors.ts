import { Store } from 'store';
import { CharactersState } from './reducer';

const getState = (state: any): CharactersState => state.characters || [];

export const getCharacters = (state: Store) => {
  const store = getState(state);
  return store.list;
};

export const getCharactersFilters = (state: Store) => {
  const store = getState(state);
  return store.filters;
};
