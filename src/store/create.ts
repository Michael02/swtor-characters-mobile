import { compose, createStore, applyMiddleware, combineReducers, Store, ReducersMapObject } from 'redux';
import thunkMiddleware from 'redux-thunk';

export default <S>(reducers: ReducersMapObject, initialState: S): Store<S> => {
  const reducer = combineReducers<S>(reducers as any);
  const middlewares = [
    thunkMiddleware,
  ];

  const composedStore = compose(
    applyMiddleware(...middlewares),
  );

  return composedStore(createStore)(
    reducer,
    initialState,
  );
};
