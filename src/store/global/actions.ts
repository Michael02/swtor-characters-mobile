import Redux from 'redux';
import * as TYPES from './constants';
import { create } from 'helpers/utils';

export const connectionChange = (payload: boolean) => (dispatch: Redux.Dispatch<any>) =>
  dispatch(create(TYPES.CONNECTION_CHANGE, payload));
