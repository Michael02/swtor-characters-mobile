import { Store } from 'store';
import { GlobalState } from './reducer';

const getState = (state: any): GlobalState => state.global || {};

export const isOnline = (state: Store) => {
  const store = getState(state);
  return store.online;
};

export const isLocked = (state: Store) => {
  const store = getState(state);
  return store.locked;
};

export const getPasscode = (state: Store) => {
  const store = getState(state);
  return store.passcode;
};
