import * as TYPES from './constants';
import { Action } from 'types/store';

export type GlobalState = {
  online: boolean,
  locked: boolean,
  passcode: string,
};

export const globalInitialState: GlobalState = {
  online: false,
  locked: false,
  passcode: '',
};

export const globalReducer = (state = globalInitialState, action: Action) => {
  const { type, payload } = action;
  switch (type) {
    case TYPES.CONNECTION_CHANGE: {
      return {
        ...state,
        online: payload,
      };
    }
    default: {
      return { ...state };
    }
  }
};
