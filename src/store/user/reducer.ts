import { Action } from 'types/store';
import * as TYPES from './constants';

export type UserState = {
  uid: string | null,
  email: string | null,
  displayName: string | null,
};

export const userInitialState: UserState = {
  uid: null,
  email: null,
  displayName: null,
};

export const userReducer = (state: UserState = userInitialState, action: Action) => {
  const { type, payload } = action;
  switch (type) {
    case TYPES.SET_USET: {
      return {
        ...state,
        ...payload,
      };
    }
    case TYPES.LOGOUT: {
      return { ...userInitialState };
    }
    default: {
      return { ...state };
    }
  }
};
