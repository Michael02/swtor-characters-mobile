import * as TYPES from './constants';
import { create } from 'helpers/utils';

export const signOut = () => create(TYPES.LOGOUT);
export const userSet = (email: string | null, uid: string | null, displayName: string | null) =>
  create(TYPES.SET_USET, { uid, email, displayName });
