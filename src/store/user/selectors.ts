import { UserState } from './reducer';
import { Store } from 'store';

const getState = (state: any): UserState => state.user || {};

export const getUID = (state: Store) => {
  const store = getState(state);
  return store.uid;
};
