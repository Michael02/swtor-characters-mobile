import { Store } from 'store';
import { OptionsState } from './reducer';

const getState = (state: any): OptionsState => state.characterOptions;

export const getServers = (state: Store) => {
  const store = getState(state);
  return store.servers;
};

export const getAllSkills = (state: Store) => {
  const store = getState(state);
  const skillsAll: string[] = [];
  if (store.skills) {
    for (const category in store.skills) {
      skillsAll.concat(store.skills[category]);
    }
  }
  return skillsAll;
};


export const getFactions = (state: Store) => {
  const store = getState(state);
  return store.factions;
};
