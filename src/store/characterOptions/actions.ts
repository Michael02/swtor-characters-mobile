import Redux from 'redux';
import * as TYPES from './constants';
import { OptionsState } from './reducer';
import { create } from 'helpers/utils';
import CharacterService from 'services/character.service';

export const characterOptionsSet = (data: OptionsState | null = null) => async(dispatch: Redux.Dispatch<any>) => {
  if (data) return dispatch(create(TYPES.OPTIONS_SET, data));
  const options = await CharacterService.getCharacterOptions();
  return dispatch(create(TYPES.OPTIONS_SET, options));
};
