import { Action } from 'types/store';
import * as TYPES from './constants';

type commonState = {
  [key: string]: string[],
};

export type serverState = {
  name: string,
  location: string,
};

export type OptionsState = {
  servers: serverState[] | null,
  factions: commonState | null,
  skills: commonState | null,
};

export const optionsInitialState: OptionsState = {
  servers: null,
  factions: null,
  skills: null,
};

export const optionsReducer = (state: OptionsState = optionsInitialState, action: Action) => {
  const { type, payload } = action;
  switch (type) {
    case TYPES.OPTIONS_SET: {
      return {
        ...state,
        servers: payload.servers,
        factions: payload.factions,
        skills: payload.skills,
      };
    }
    default: {
      return { ...state };
    }
  }
};
