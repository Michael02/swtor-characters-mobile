import createStore from './create';
import { globalReducer, globalInitialState } from './global/reducer';
import { userReducer, userInitialState } from './user/reducer';
import { charactersReducer, charactersInitialState } from './characters/reducer';
import { optionsReducer, optionsInitialState } from './characterOptions/reducer';

const reducers = {
  global: globalReducer,
  user: userReducer,
  characters: charactersReducer,
  characterOptions: optionsReducer,
};

const initialState = {
  global: globalInitialState,
  user: userInitialState,
  characters: charactersInitialState,
  characterOptions: optionsInitialState,
};


const store = createStore(reducers, initialState);

export type Store = typeof store;
export default store;
