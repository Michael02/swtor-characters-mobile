import React from 'react';
import { View, Text, StyleSheet, ViewStyle, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProps } from 'react-navigation';
import EmailValidator from 'email-validator';

import BackgroundGradient from 'components/BackgroundGradient';
import { Store } from 'store';
import { get } from 'helpers/translations';
import AuthService from 'services/auth.service';
import { MIN_PASSWORD_LEN } from 'config';
import LoginInput from 'components/LoginInput';

const select = (state: Store) => ({});
const mapActions = {};

type MappedProps =  ReturnType<typeof select>;
type MappedActions =  typeof mapActions;

type Props = {} & NavigationScreenProps<any> & MappedProps & MappedActions;
type State = {
  displayName: string,
  email: string,
  password: string,
  passwordConfirm: string,
};

class Signup extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      displayName: '',
      email: '',
      password: '',
      passwordConfirm: '',
    };
  }
  refDisplayNameInput = React.createRef<any>();
  refEmailInput = React.createRef<any>();
  refPasswordInput = React.createRef<any>();
  refPasswordConfirmInput = React.createRef<any>();

  get isValid() {
    const { email, password, passwordConfirm } = this.state;
    return EmailValidator.validate(email) && password.length >= MIN_PASSWORD_LEN && password === passwordConfirm;
  }

  changeDisplayName = (displayName: string) => this.setState({ displayName });
  changeEmail = (email: string) => this.setState({ email });
  changePassword = (password: string) => this.setState({ password });
  changePasswordConfirm = (passwordConfirm: string) => this.setState({ passwordConfirm });

  renderInputs = () => {
    const { displayName, email, password, passwordConfirm } = this.state;
    const inputs = [
      {
        value: displayName,
        placeholder: get('signup.display_name'),
        icon: 'user_circle',
        onChange: (t: string) => this.changeDisplayName(t),
        inputProps: {
          onSubmitEditing: () => this.refEmailInput.current.focus(),
          returnKeyType: 'next',
          autoFocus: true,
        },
        ref: this.refDisplayNameInput,
      },
      {
        value: email,
        placeholder: get('signup.email'),
        icon: 'at_sign',
        onChange: (t: string) => this.changeEmail(t),
        inputProps: {
          onSubmitEditing: () => this.refPasswordInput.current.focus(),
          returnKeyType: 'next',
        },
        ref: this.refEmailInput,
      },
      {
        value: password,
        placeholder: get('signup.password'),
        icon: 'key',
        onChange: (t: string) => this.changePassword(t),
        inputProps: {
          onSubmitEditing: () => this.refPasswordConfirmInput.current.focus(),
          returnKeyType: 'next',
          secureTextEntry: true,
        },
        ref: this.refPasswordInput,
      },
      {
        value: passwordConfirm,
        placeholder: get('signup.password_confirm'),
        icon: 'key',
        onChange: (t: string) => this.changePasswordConfirm(t),
        inputProps: {
          onSubmitEditing: () => this.onSubmit(),
          returnKeyType: 'go',
          secureTextEntry: true,
        },
        ref: this.refPasswordConfirmInput,
      },
    ];

    return inputs.map((item: any, index: number) => <LoginInput config={ item } key={ index }/>);
  }

  onSubmit = () => {
    const { displayName, email, password } = this.state;
    if (this.isValid) {
      AuthService.signup(email, password, displayName);
    }
  }

  render() {
    return (
      <BackgroundGradient style={ styles.container }>
        <View style={ styles.inputsWrapper }>
          { this.renderInputs() }
        </View>
        <TouchableOpacity
          onPress={ () => this.props.navigation.goBack() }
        >
          <Text>back to login</Text>
        </TouchableOpacity>
      </BackgroundGradient>
    );
  }
}

type styles = {
  container: ViewStyle,
  inputsWrapper: ViewStyle,
};

const styles = StyleSheet.create<styles>({
  container: {
    flex: 1,
    paddingHorizontal: 15,
  },
  inputsWrapper: {
    marginTop: 15,
  },
});

export default connect(select, mapActions)(Signup);
