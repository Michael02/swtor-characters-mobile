/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, TextStyle, ViewStyle } from 'react-native';
import BackgroundGradient from 'components/BackgroundGradient';

const instructions = Platform.select({
  // tslint:disable:prefer-template
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press ctrl+M for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <BackgroundGradient style={ styles.container }>
        <Text style={ styles.welcome }>Welcome to React Native!</Text>
        <Text style={ styles.instructions }>To get started, edit Main.tsx</Text>
        <Text style={ styles.instructions }>{ instructions }</Text>
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,
  welcome: TextStyle,
  instructions: TextStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
