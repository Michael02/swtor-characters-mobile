import React from 'react';
import { View, StyleSheet, ViewStyle, TextStyle, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProps } from 'react-navigation';
import EmailValidator from 'email-validator';

import BackgroundGradient from 'components/BackgroundGradient';
import { Store } from 'store';
import { GOLD } from 'styles/global';
import Button from 'components/Button';
import { isOnline } from 'store/global';
import AuthService from 'services/auth.service';
import { MIN_PASSWORD_LEN } from 'config';
import { get } from 'helpers/translations';
import LoginInput from 'components/LoginInput';
import { MAIN, SIGNUP } from 'constants/routes';

const select = (state: Store) => ({
  isOnline: isOnline(state),
});
const mapActions = {};

type MappedProps =  ReturnType<typeof select>;
type MappedActions =  typeof mapActions;

type Props = {} & NavigationScreenProps<any> & MappedProps & MappedActions;
type State = {
  email: string,
  password: string,
};

class Login extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }
  refPasswordInput = React.createRef<any>();

  emailChange = (email: string) => this.setState({ email });
  passwordChange = (password: string) => this.setState({ password });

  get valid() {
    const { email, password } = this.state;
    return password.length >= MIN_PASSWORD_LEN && EmailValidator.validate(email);
  }

  onSubmit = async() => {
    console.log('login');
    if (this.valid) {
      const { email, password } = this.state;
      const loged = await AuthService.login(email, password);
      if (loged) this.props.navigation.navigate(MAIN);
    }
  }

  renderInputs = () => {
    const { email, password } = this.state;
    const inputs = [
      {
        value: email,
        placeholder: get('login.email'),
        icon: 'at_sign',
        onChange: (t: string) => this.emailChange(t),
        inputProps: {
          onSubmitEditing: () => this.refPasswordInput.current.focus(),
          returnKeyType: 'next',
          autoFocus: true,
        },
      },
      {
        value: password,
        placeholder: get('login.password'),
        icon: 'key',
        onChange: (t: string) => this.passwordChange(t),
        inputProps: {
          onSubmitEditing: () => this.onSubmit(),
          returnKeyType: 'go',
          secureTextEntry: true,
        },
        ref: this.refPasswordInput,
      },
    ];
    return inputs.map((item: any, index: number) => <LoginInput config={ item } key={ index }/>);
  }

  render() {
    return (
      <BackgroundGradient style={ styles.container }>
        <View>
          <Text style={ styles.title }>SWTOR Characters</Text>
          { this.renderInputs() }
          <View style={ styles.signupWrapper }>
            <Text style={ styles.signupText }>Don't have account yet</Text>
            <TouchableOpacity
              onPress={ () => this.props.navigation.navigate(SIGNUP) }
            >
              <Text style={ styles.signupButton }>Click here to Sign Up</Text>
            </TouchableOpacity>
          </View>
        </View>
          <Button
            text="Log In"
            onPress={ () => this.onSubmit() }
            disabled={ !this.valid }
            styleButton={ styles.button }
            styleText={ styles.buttonText }
          />
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,
  title: TextStyle,
  buttonWrapper: ViewStyle,
  button: ViewStyle,
  buttonText: TextStyle,
  signupWrapper: ViewStyle,
  signupText: TextStyle,
  signupButton: TextStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    justifyContent: 'space-between',
  },
  title: {
    marginTop: 15,
    marginBottom: 15,
    fontFamily: 'AurekBesh',
    fontSize: 12,
    textAlign: 'center',
    color: GOLD,
  },
  buttonWrapper: {
    justifyContent: 'flex-end',
    borderWidth: 1,
    borderColor: '#0f0',
  },
  button: {
    marginBottom: 20,
  },
  buttonText: {
    fontFamily: 'Roboto',
  },
  signupWrapper: {
    alignItems: 'center',
  },
  signupText: {
    color: GOLD,
    fontFamily: 'Roboto',
    textAlign: 'center',
    fontSize: 11,
  },
  signupButton: {
    color: GOLD,
  },
});

export default connect(select, mapActions)(Login);
