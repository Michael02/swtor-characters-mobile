import React from 'react';
import { StyleSheet, ViewStyle, Text } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProps } from 'react-navigation';
import BackgroundGradient from 'components/BackgroundGradient';

const select = (state: any) => ({});
const mapActions = {};

type mappedProps = ReturnType<typeof select>;
type mappedActions = typeof mapActions;

type Props = {} & NavigationScreenProps<any> & mappedProps & mappedActions;
type State = {
  oldPassword: string,
  newPassword: string,
  confirmPassword: string,
};

class PasswordChange extends React.Component<Props, State> {
  render() {
    return (
      <BackgroundGradient style={ styles.container }>
        <Text>PasswordChange</Text>
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
  },
});

export default connect(select, mapActions)(PasswordChange);
