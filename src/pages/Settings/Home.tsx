import React from 'react';
import { Text, StyleSheet, ViewStyle } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProps } from 'react-navigation';

import BackgroundGradient from 'components/BackgroundGradient';
import { Store } from 'store';
import Button from 'components/Button';
import * as storage from 'helpers/storage';
import { signOut } from 'store/user';
import { LOGIN } from 'constants/routes';

const select = (state: Store) => ({});
const mapActions = {
  signOut,
};

type MappedProps =  ReturnType<typeof select>;
type MappedActions =  typeof mapActions;

type Props = {} & NavigationScreenProps<any> & MappedProps & MappedActions;
type State = {};

class Settings extends React.Component<Props, State> {
  nuke = () => {
    storage.clear();
    this.props.signOut();
    this.props.navigation.navigate(LOGIN);
  }

  render() {
    return (
      <BackgroundGradient fixed={ true } style={ styles.container }>
        <Text>Settings</Text>
        <Button
          text={ 'Clear all data' }
          onPress={ () => this.nuke() }
        />
      </BackgroundGradient>
    );
  }
}

type styles = {
  container: ViewStyle,
};

const styles = StyleSheet.create<styles>({
  container: {
    flex: 1,
  },
});

export default connect(select, mapActions)(Settings);
