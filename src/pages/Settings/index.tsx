import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import * as ROUTES from 'constants/routes';
import { styles } from 'styles/header';
import Home from './Home';
import PasswordChange from './PasswordChange';
import { get } from 'helpers/translations';
import NavigationService from 'services/navigation.service';
import Icon from 'components/Icon';
import { WHITE } from 'styles/global';

const settingsNavigator = createStackNavigator(
  {
    [ROUTES.SETTINGS_HOME]: {
      screen: Home,
      navigationOptions: {
        title: get('label.settings'),
        headerLeft: (
          <View style={ styles.headerLeft }/>
        ),
      },
    },
    [ROUTES.SETTINGS_PASSWORD_CHANGE]: {
      screen: PasswordChange,
      navigationOptions: {
        title: get('label.password_change'),
      },
    },
  },
  {
    initialRouteName: ROUTES.SETTINGS_HOME,
    navigationOptions: {
      gesturesEnabled: false,
      headerStyle: styles.header,
      headerTitleStyle: styles.headerTitle,
      headerLeft: (
        <TouchableOpacity
          style={ styles.headerLeft }
          onPress={ () => NavigationService.goBack() }
        >
          <Icon
            name="arrow_left"
            size={ 20 }
            color={ WHITE }
            style={ {
              width: 20,
              height: 20,
            } }
          />
        </TouchableOpacity>
      ),
      headerRight: (
        <View style={ styles.headerRight }/>
      ),
    },
  },
);

export default settingsNavigator;
