import React from 'react';
import { NavigationScreenProps } from 'react-navigation';
import { connect } from 'react-redux';
import { Text, StyleSheet, ViewStyle, TextStyle, Image, ImageStyle } from 'react-native';
import { MAIN, UNAUTHORIZED, DISCONNECTED } from 'constants/routes';
import { delay } from 'helpers/utils';
import BackgroundGradient from 'components/BackgroundGradient';
import { SWTOR_LOGO } from 'assets/images';
import { getWindowWidth } from 'helpers/size';
import * as storage from 'helpers/storage';
import { USER_PROFILE } from 'constants/keys';
import { GOLD } from 'styles/global';
import { isOnline } from 'store/global';
import { Store } from 'store';
import AuthService from 'services/auth.service';
import { getUID } from 'store/user';
import * as crypto from 'helpers/crypto';
import { characterOptionsSet } from 'store/characterOptions';
import { fetchCharacters } from 'store/characters';

const select = (state: Store) => ({
  onLine: isOnline(state),
  uid: getUID(state),
});
const mapActions = {
  characterOptionsSet,
  fetchCharacters,
};

type mappedProps = ReturnType<typeof select>;
type mappedActions = typeof mapActions;

type Props = {} & NavigationScreenProps<any> & mappedProps & mappedActions;
type State = {};

class Splash extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.init();
  }

  init = async() => {
    // await AuthService.signout();  // TODO turn off
    await AuthService.checkAuth();
    const route = await this.getInitialRoute();
    await delay(150);
    this.props.navigation.navigate(route);
  }

  getInitialRoute = async() => {
    const userProfile = await storage.load(USER_PROFILE);
    if (!userProfile && !this.props.onLine && !this.props.uid) return DISCONNECTED;
    if (!userProfile && !this.props.uid) return UNAUTHORIZED;
    if (userProfile && !this.props.uid) {
      try {
        const userPassword = crypto.decrypt(userProfile.password);
        const loged = await AuthService.login(userProfile.email, userPassword);
        if (!loged) return UNAUTHORIZED;
      } catch {
        return UNAUTHORIZED;
      }
    }
    if (this.props.onLine) this.initialFetch();
    return MAIN;
  }

  initialFetch = async() => {
    await this.props.characterOptionsSet();
    await this.props.fetchCharacters();
  }

  render() {
    return (
      <BackgroundGradient style={ styles.container } fixed={ true }>
        <Image
          source={ SWTOR_LOGO }
          style={ styles.logo }
        />
        <Text style={ styles.text }>swtor_characters_mobile</Text>
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,
  logo: ImageStyle,
  text: TextStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: getWindowWidth() * 0.9,
    height: getWindowWidth() * 0.9 * 0.36,
    marginBottom: 50,
  },
  text: {
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
    color: GOLD,
  },
});

export default connect(select, mapActions)(Splash);
