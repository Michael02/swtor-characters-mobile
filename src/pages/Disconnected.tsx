import React from 'react';
import { Text, ViewStyle, TextStyle, StyleSheet }  from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import Icon from 'components/Icon';
import BackgroundGradient from 'components/BackgroundGradient';
import { Store } from 'store';
import { isOnline } from 'store/global';
import NavigationService from 'services/navigation.service';
import { LOGIN } from 'constants/routes';

const select = (state: Store) => ({
  online: isOnline(state),
});

type mappedProps = ReturnType<typeof select>;

type Props = {} & NavigationScreenProps<any> & mappedProps;
type State = {};

class Disconnected extends React.Component<Props, State> {
  componentDidUpdate() {
    if (this.props.online) NavigationService.navigate(LOGIN);
  }

  render() {
    return (
      <BackgroundGradient fixed={ true }>
        <Icon
          name="disconnected"
          style={ styles.icon }
          size={ 60 }
        />
        <Text style={ styles.text }>{ '' }</Text>
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,
  icon: ViewStyle,
  text: TextStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    alignItems: 'center',
    paddingTop: 50,
  },
  icon: {
    marginBottom: 30,
  },
  text: {},
});

export default Disconnected;
