import React from 'react';
import { Text, ViewStyle, StyleSheet } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { connect } from 'react-redux';
import { Store } from 'store';
import { CharacterState } from 'store/characters/reducer';
import BackgroundGradient from 'components/BackgroundGradient';
import CharacterImage from 'components/CharacterImage';

const select = (state: Store) => ({});
const mapActions = {};

type mappedProps = ReturnType<typeof select>;
type mappedActions = typeof mapActions;

type Props = {} & NavigationScreenProps & mappedProps & mappedActions;
type State = Partial<CharacterState> & {
  edit: boolean,
  lvl: string,
  commanderLvl: string,
};

class CharacterEdit extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      edit: false,
      lvl: '1',
      commanderLvl: '1',
    };
  }

  componentWillMount = () => {
    const { navigation } = this.props;
    const character: CharacterState = navigation.getParam('character', null);
    if (!character) navigation.goBack();
    this.setState({
      server: character.server,
      faction: character.faction,
      name: character.name,
      className: character.className,
      level: character.level,
      commanderLevel: character.commanderLevel,
      images: character.images,
      crewSkills: character.crewSkills,
    });
  }

  render() {
    return (
      <BackgroundGradient style={ styles.container }>
        <Text>Character Edit</Text>
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
  },
});

export default connect(select, mapActions)(CharacterEdit);
