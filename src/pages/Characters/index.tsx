import React from 'react';
import { createStackNavigator, NavigationScreenConfig } from 'react-navigation';
import * as ROUTES from 'constants/routes';
import CharactersList from './CharactersList';
import CharacterEdit from './CharacterEdit';
import { View, TouchableOpacity } from 'react-native';
import CharacterAdd from './CharacterAdd';
import Icon from 'components/Icon';
import { styles } from 'styles/header';
import { WHITE } from 'styles/global';
import { get } from 'helpers/translations';
import NavigationService from 'services/navigation.service';

const CharactersNavigator = createStackNavigator(
  {
    [ROUTES.CHARACTERS_LIST]: {
      screen: CharactersList,
      navigationOptions: {
        title:get('label.characters_list'),
        headerLeft: (
          <View style={ styles.headerLeft }/>
        ),
      },
    },
    [ROUTES.CHARACTER_EDIT]: {
      screen: CharacterEdit,
      navigationOptions: ({ navigation }: NavigationScreenConfig<any>) => {
        const character = navigation.getParam('character', null);
        const name = character ? character['name'] : null;
        return {
          title: name ? name : get('label.character_edit'),
        };
      },
    },
    [ROUTES.CHARACTER_ADD]: {
      screen: CharacterAdd,
      navigationOptions: {
        title: get('label.character_add'),
      },
    },
  },
  {
    initialRouteName: ROUTES.CHARACTERS_LIST,
    navigationOptions: {
      gesturesEnabled: false,
      headerStyle: styles.header,
      headerTitleStyle: styles.headerTitle,
      headerLeft: (
        <TouchableOpacity
          style={ styles.headerLeft }
          onPress={ () => NavigationService.goBack() }
        >
          <Icon
            name="arrow_left"
            size={ 20 }
            color={ WHITE }
            style={ {
              width: 20,
              height: 20,
            } }
          />
        </TouchableOpacity>
      ),
      headerRight: (
        <View style={ styles.headerRight }/>
      ),
    },
  },
);

export default CharactersNavigator;
