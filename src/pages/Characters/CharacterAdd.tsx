import React from 'react';
import { StyleSheet, ViewStyle, Text, View, TextInput, TextStyle, TouchableOpacity } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { connect } from 'react-redux';
import { CharacterState } from 'store/characters/reducer';
import BackgroundGradient from 'components/BackgroundGradient';
import { characterAdd } from 'store/characters';
import { isOnline } from 'store/global';
import { lower } from 'helpers/text';
import { ICONS } from 'assets/icons';
import { serverState } from 'store/characterOptions/reducer';
import { ListItem } from 'types/listItem';
import PickerModal from 'components/PickerModal';
import { AddItem } from 'types/character';
import Button from 'components/Button';
import { EMP_RED, REP_BLUE, GOLD_DARK, WHITE, GOLD, GRAY, GRAY_DARK } from 'styles/global';
import Icon from 'components/Icon';
import { removeIndex, keys } from 'helpers/array';
import { IconName } from 'types/icon';
import { get } from 'helpers/translations';
import { md5 } from 'helpers/crypto';

const select = (state: any) => {
  const { characterOptions } = state;
  let skillsAll: string[] = [];
  if (characterOptions.skills) {
    for (const category in characterOptions.skills) {
      skillsAll = skillsAll.concat(characterOptions.skills[category]);
    }
  }
  return ({
    factions: characterOptions.factions,
    servers: characterOptions.servers,
    skills: skillsAll,
    online: isOnline(state),
  });
};
const mapActions = {
  characterAdd,
};

type mappedProps = ReturnType<typeof select>;
type mappedActions = typeof mapActions;

type Props = {} & NavigationScreenProps & mappedProps & mappedActions;
type State = Partial<CharacterState> & {
  picker: 'server' | 'faction' | 'className' | 'crewSkills' | null,
  pickerData: ListItem[],
  lvl: string,
  commanderLvl: string,
};

class CharacterAdd extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      server: '',
      faction: '',
      className: '',
      name: '',
      lvl: '1',
      commanderLvl: '1',
      crewSkills: [],
      images: [],
      picker: null,
      pickerData: [],
    };
  }

  get factionsData() {
    const { factions } = this.props;
    return keys(factions).map((item: string) =>
      ({
        label: item,
        icon: {
          name: lower(item).includes('sith') ? ICONS.sith_empire : ICONS.old_republic,
          color: lower(item).includes('sith') ? EMP_RED : REP_BLUE,
        },
        selected: item === this.state.faction,
      }));
  }

  get serversData() {
    const { servers } = this.props;
    return servers.map((item: serverState) =>
      ({ label: item.name, info: item.location, selected: item.name === this.state.server }));
  }

  get skillsData() {
    const { skills } = this.props;
    const { crewSkills } = this.state;
    return skills.map((item: string) => ({ label: item, checked: crewSkills!.indexOf(item) !== -1 }));
  }

  get classesData() {
    const { factions } = this.props;
    if (!this.state.faction) return [];
    const classes = factions[this.state.faction];
    return classes.map((item: string) => ({ label: item, selected: item === this.state.className }));
  }

  changeName = (name: string) => { this.setState({ name }); };
  changeLevel = (lvl: string) => {
    if (lvl === '' || (parseInt(lvl, 10) > 0 && parseInt(lvl, 10) <= 70)) this.setState({ lvl });
  }
  changeComanderLevel = (commanderLvl: string) => {
    if (commanderLvl === '' || (parseInt(commanderLvl, 10) > 0 && parseInt(commanderLvl, 10) <= 300)) this.setState({ commanderLvl });
  }

  get addItems(): AddItem[] {
    return [
      {
        label: get('character_add.server'),
        property: 'server',
        type: 'button',
        render: true,
        onPress: () => this.openPicker('server'),
        value: this.state.server || get('button.choose'),
      },
      {
        label: get('character_add.faction'),
        property: 'faction',
        type: 'button',
        render: true,
        onPress: () => this.openPicker('className'),
        value: this.state.faction || get('button.choose'),
      },
      {
        label: get('character_add.class_name'),
        property: 'className',
        type: 'button',
        render: !!this.state.faction,
        onPress: () => this.openPicker('className'),
        value: this.state.className || get('button.choose'),
      },
      {
        label: get('character_add.name'),
        property: 'name',
        type: 'input',
        render: true,
        value: this.state.name,
        onChange: (t: string) => this.changeName(t),
      },
      {
        label: get('character_add.level'),
        property: 'lvl',
        type: 'number',
        render: true,
        value: `${ this.state.lvl }`,
        onChange: (t: string) => this.changeLevel(t),
      },
      {
        label: get('character_add.commander_level'),
        property: 'commanderLvl',
        type: 'number',
        render: this.state.lvl === '70',
        value: `${ this.state.commanderLvl }`,
        onChange: (t: string) => this.changeComanderLevel(t),
      },
      {
        label: get('character_add.crew_skills'),
        property: 'crewSkills',
        type: 'button',
        render: true,
        value: `${ this.state.crewSkills!.map((item: string) => `${item} `) }`,
        onPress: () => this.openPicker('crewSkills'),
      },
    ];
  }

  handlePickerSelect = (text: string) => {
    const { picker } = this.state;
    if (!picker) return;
    if (picker === 'faction' && text !== this.state.faction) this.setState({ className: '' });
    if (picker === 'crewSkills') {
      if (this.state.crewSkills === null) this.setState({ crewSkills: [text] });
      else if (this.state.crewSkills!.length <= 3) {
        let skills = this.state.crewSkills;
        const index = skills!.indexOf(text);
        if (skills!.length === 3 && index === -1) return; // TODO rize error
        if (index === -1) skills!.push(text);
        else skills = removeIndex(skills!, index);
        this.setState({ crewSkills: skills });
        this.setState({ pickerData: this.skillsData });
        if (skills!.length === 3) this.setState({ picker: null, pickerData: [] });
      }
    } else this.setState({ [picker]: text, picker: null, pickerData: [] } as any);
  }

  openPicker = (label: 'server' | 'faction' | 'crewSkills' | 'className') => {
    switch (label) {
      case 'server': {
        this.setState({ picker: label, pickerData: this.serversData });
        break;
      }
      case 'faction': {
        this.setState({ picker: label, pickerData: this.factionsData as any });
        break;
      }
      case 'crewSkills': {
        this.setState({ picker: label, pickerData: this.skillsData });
        break;
      }
      case 'className': {
        this.setState({ picker: label, pickerData: this.classesData });
        break;
      }
    }
  }

  renderAddItem = (item: AddItem, key: number) => {
    const {
      type,
      label,
      property,
      render,
      value = '',
      onChange = () => null,
    } = item;

    if (!render) return null;

    let input;

    switch (type) {
      case 'button': {
        input = (
          <Button
            text={ value  as string }
            styleButton={ styles.addItemButton as any }
            styleText={ styles.addItemButtonText as any }
            onPress={ () => this.openPicker(property as any) }
            gradient={ false }
          />
        );
        break;
      }
      case 'input': {
        input = (
          <TextInput
            value={ value as string }
            onChangeText={ (t: string) => onChange(t) }
            autoCapitalize="none"
            autoCorrect={ false }
            autoFocus={ false }
            style={ styles.addItemInput }
          />
        );
        break;
      }
      case 'number': {
        const level = parseInt(value, 10);
        input = (
          <View style={ styles.addItemNumberInput }>
            <TouchableOpacity
              style={ styles.minusButtonWrapper }
              onPress={ () => onChange((level - 1).toString()) }
            >
              <Icon
                name={ ICONS.minus as IconName }
                size={ 20 }
                style={ styles.numberInputButton as any }
              />
            </TouchableOpacity>
            <TextInput
              value={ value }
              onChangeText={ (t: string) => onChange(t) }
              autoCapitalize="none"
              autoCorrect={ false }
              autoFocus={ false }
              style={ styles.numberInput }
              keyboardType="number-pad"
              maxLength={ 3 }
              placeholder="1"
            />
            <TouchableOpacity
              style={ styles.plusButtonWrapper }
              onPress={ () => onChange((level + 1).toString()) }
            >
              <Icon
                name={ ICONS.add as IconName }
                size={ 20 }
                style={ styles.numberInputButton as any }
              />
            </TouchableOpacity>
          </View>
        );
        break;
      }
    }

    return (
      <View style={ styles.addItemWrapperr } key={ key }>
        <Text style={ styles.addItemLabel }>{ label }</Text>
        { input }
      </View>
    );
  }

  get isValid() {
    const { server, faction, className, name, lvl, commanderLvl } = this.state;
    return server && faction && className && name && lvl && commanderLvl;
  }

  onSubmit = async() => {
    console.log('onSubmit: valid: ', this.isValid);
    if (!this.isValid) return;
    const { server = '', faction = '', className = '', name = '', lvl, commanderLvl, images = [], crewSkills = [] } = this.state;
    const cid = md5(`${ name } ${ server }`);
    const newCharacter: CharacterState = {
      cid,
      server,
      faction,
      className,
      name,
      level: parseInt(lvl, 10),
      commanderLevel: parseInt(commanderLvl, 10),
      images,
      crewSkills,
      created: new Date().toISOString(),
      modified: new Date().toISOString(),
    };
    const saved = await this.props.characterAdd(newCharacter);
    if (saved) {
      this.props.navigation.goBack();
    } else {
      // TODO show error
    }
  }

  render() {
    const { picker, pickerData } = this.state;
    return (
      <BackgroundGradient style={ styles.container }>
        <View>
          { this.addItems.map((item, index) => this.renderAddItem(item, index)) }
        </View>
        <View style={ styles.buttonWrapper }>
          <Button
            text={ get('character_add.button') }
            onPress={ () => this.onSubmit() }
            styleButton={ styles.button }
            styleText={ styles.buttonText }
            // disabled={ !this.isValid }
          />
        </View>
        <PickerModal
          visible={ !!picker }
          data={ pickerData }
          onSelect={ (t: string) => this.handlePickerSelect(t) }
          onClose={ picker === 'crewSkills' ? () => this.setState({ picker: null, pickerData: [] }) : null }
        />
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,

  addItemWrapperr: ViewStyle,
  addItemLabel: TextStyle,
  addItemButton: ViewStyle,
  addItemButtonText: TextStyle,
  addItemInput: TextStyle,
  addItemNumberInput: ViewStyle,
  numberInput: TextStyle,
  numberInputButton: ViewStyle,
  minusButtonWrapper: ViewStyle,
  plusButtonWrapper: ViewStyle,

  buttonWrapper: ViewStyle,
  button: ViewStyle,
  buttonText: TextStyle,
};

const inputStyle: ViewStyle = {
  borderColor: GOLD_DARK,
  backgroundColor: GRAY,
  borderWidth: 2,
  borderRadius: 5,
  height: 38,
  paddingHorizontal: 10,
  paddingVertical: 2,
  flex: 1,
  marginLeft: 10,
};

const numberButton: ViewStyle = {
  width: 30,
  height: 30,
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 15,
  backgroundColor: GOLD,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
    paddingTop: 15,
    justifyContent: 'space-between',
  },
  addItemWrapperr: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    height: 40,
    marginBottom: 10,
  },
  addItemLabel: {
    fontSize: 20,
    lineHeight: 22,
  },
  addItemButton: {
    ...inputStyle,
    paddingHorizontal: 0,
    paddingVertical: 0,
  },
  addItemButtonText: {},
  addItemInput: {
    ...inputStyle,
    color: WHITE,
    textAlign: 'center',
    flex: 1,
  },
  addItemNumberInput: {
    ...inputStyle,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  numberInput: {
    width: 50,
    height: 30,
    color: WHITE,
    fontSize: 18,
    lineHeight: 20,
    padding: 0,
    textAlign: 'center',
    backgroundColor: GRAY_DARK,
    borderRadius: 5,
  },
  numberInputButton: {},
  minusButtonWrapper: {
    ...numberButton,
    marginRight: 10,
  },
  plusButtonWrapper: {
    ...numberButton,
    marginLeft: 10,
  },

  buttonWrapper: {
    paddingHorizontal: 15,
    marginBottom: 10,
  },
  button: {
    borderRadius: 15,
  },
  buttonText: {},
});

export default connect(select, mapActions)(CharacterAdd);
