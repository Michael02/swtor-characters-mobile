import React from 'react';
import { FlatList, StyleSheet, ViewStyle, Text, TouchableOpacity, TextStyle } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { connect } from 'react-redux';
import { Store } from 'store';
import { getCharacters, getCharactersFilters } from 'store/characters';
import BackgroundGradient from 'components/BackgroundGradient';
import CharacterItem from 'components/CharacterItem';
import { isOnline } from 'store/global';
import Spinner from 'components/Spinner';
import { REP_BLUE, GOLD } from 'styles/global';
import Icon from 'components/Icon';
import { get } from 'helpers/translations';
import { CHARACTER_ADD, CHARACTER_EDIT } from 'constants/routes';
import { getWindowWidth } from 'helpers/size';
import { CharacterState } from 'store/characters/reducer';
import { keys } from 'helpers/array';

const select = (state: Store) => {
  const { characterOptions }: any = state;
  return ({
    characters: getCharacters(state),
    filters: getCharactersFilters(state),
    online: isOnline(state),
    characterOptions: characterOptions.servers !== null &&
      characterOptions.factions !== null &&
      characterOptions.skills !== null,
  });
};
const mapActions = {};

type mappedProps = ReturnType<typeof select>;
type mappedActions = typeof mapActions;

type Props = {} & NavigationScreenProps<any> & mappedProps & mappedActions;
type State = {
  data: CharacterState[],
  loading: boolean,
};

const addColor = GOLD;

class CharacterList extends React.Component<Props, State> {
  static navigationOptions = ({ navigation }: any) => {
    const online = navigation.getParam('online', true);
    console.log('navigationOptions online: ', online);
    if (!online) return ({ headerRight: null });
    return ({});
  }

  constructor(props: Props) {
    super(props);
    const { characters } = this.props;
    const data = characters;
    this.state = {
      data,
      loading: true,
    };
  }

  componentDidMount = () => {
    const { data } = this.state;
    if (data) this.setState({ loading: false });

    const { online, navigation, characterOptions } = this.props;
    if (!online || !characterOptions) navigation.setParams({ online: false });
    else navigation.setParams({ online });
  }

  componentDidUpdate = (prevProps: Props) => {
    const { online, navigation, characterOptions, characters } = this.props;
    if (characters.length !== prevProps.characters.length) this.setState({ data: characters });
    if (online === prevProps.online && characterOptions === prevProps.characterOptions) return;

    const navOnline = navigation.getParam('online', true);
    if (navOnline && (!online || !characterOptions)) navigation.setParams({ online: false });
    else if (!navOnline && online && characterOptions) navigation.setParams({ online: true });
  }

  handlePress = (character: CharacterState) => {
    console.log(`prassed character id = ${ character }`);
    if (!character || !keys(character).length) return;
    const { navigation } = this.props;
    navigation.navigate(CHARACTER_EDIT, { character });
  }

  sortCharacters = () => {
    const { characters, filters } = this.props;
    const { sortBy } = filters;
    // tslint:disable-next-line:prefer-array-literal
    let result = new Array();
    switch (sortBy) {
      case 'modified': {
        result = characters.sort((obj1, obj2) =>
          new Date(obj1.modified).getTime() > new Date(obj2.modified).getTime() ? -1 :
          new Date(obj1.modified).getTime() < new Date(obj2.modified).getTime() ? 1 : 0);
        break;
      }
      case 'level': {
        result = characters.sort((obj1, obj2) =>
          obj1.level > obj2.level ? -1 :
          obj1.level < obj2.level ? 1 : 0);
        break;
      }
      case 'name': {
        result = characters.sort((obj1, obj2) =>
          obj1.name > obj2.name ? -1 :
          obj1.name < obj2.name ? 1 : 0);
        break;
      }
    }
    return result;
  }

  getFilterCharacters = () => {
    const { filters } = this.props;
    const { className, server, faction, crewSkills } = filters;
    let { characters } = this.props;

    if (faction) characters = characters.filter((item) => item.faction === faction);
    if (server) characters = characters.filter((item) => item.server === server);
    if (className) characters = characters.filter((item) => item.className === className);
    if (crewSkills.length) characters = characters.filter((item) => crewSkills.every((skill) => item.crewSkills.indexOf(skill) !== -1));

    return characters;
  }

  renderAddButton = () => (
    <TouchableOpacity
      onPress={ () => this.props.navigation.navigate(CHARACTER_ADD) }
      style={ styles.buttonAdd }
    >
      <Icon
        name="user_plus"
        color={ addColor }
        size={ 30 }
        style={ styles.buttonIcon }
      />
      <Text style={ styles.buttonText }>{ get('characters_list.add_character') }</Text>
    </TouchableOpacity>
  )

  render() {
    const { data, loading } = this.state;
    console.log('Characters List state: ', this.state);
    const { characterOptions, online } = this.props;
    if (loading) {
      return (
        <BackgroundGradient fixed={ true } style={ styles.container }>
          <Spinner color={ REP_BLUE } size={ 60 }/>
        </BackgroundGradient>
      );
    }
    return (
      <BackgroundGradient style={ styles.container }>
        <FlatList
          data={ data }
          style={ styles.list }
          keyExtractor={ (item) => item.cid }
          renderItem={ ({ item, index }) =>
            <CharacterItem character={ item } onPress={ () => this.handlePress(item) } last={ index === data.length - 1 }/>
          }
        />
          { online && characterOptions && this.renderAddButton() }
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,
  list: ViewStyle,

  buttonAdd: ViewStyle,
  buttonIcon: ViewStyle,
  buttonText: TextStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
  },
  list: {
    paddingTop: 10,
  },



  buttonAdd :{
    width: getWindowWidth() - 30,
    height: 50,
    marginHorizontal: 15,
    borderColor: addColor,
    borderWidth: 1,
    borderRadius: 25,
    paddingHorizontal: 15,
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 15,
  },
  buttonIcon :{
    paddingRight: 15,
    borderRightColor: addColor,
    borderRightWidth: 1,
    marginRight: 15,
    width: 50,
    height: 45,
  },
  buttonText :{
    color: addColor,
    fontSize: 24,
  },
});

export default connect(select, mapActions)(CharacterList);
