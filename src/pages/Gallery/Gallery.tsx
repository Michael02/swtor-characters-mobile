import React from 'react';
import { View, Image, Text, StyleSheet, ViewStyle, TouchableOpacity } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { connect } from 'react-redux';
import BackgroundGradient from 'components/BackgroundGradient';
import CharacterImage from 'components/CharacterImage';
import { get } from 'helpers/translations';
import { styles as headerStyles } from 'styles/header';
import { getWindowWidth } from 'helpers/size';

const select = (state: any) => ({});
const mapActions = {};

type mappedProps = ReturnType<typeof select>;
type mappedActions = typeof mapActions;

type Props = {} & NavigationScreenProps<any> & mappedProps & mappedActions;
type State = {};

class Gallery extends React.Component<Props, State> {
  static navgationOptions = ({ navigation }: NavigationScreenProps<any>) => {
    // return {
      
    // };
  }

  renderImage = (url: string, index: number) => (
    <TouchableOpacity
      style={ styles.imageWrapper }
      onPress={ () => this.setState({ imageIndex: index }) }
    >
      <CharacterImage
        url={ url }
        style={ styles.image }
      />
    </TouchableOpacity>
  )

  render() {
    return (
      <BackgroundGradient style={ styles.container }>
        <Text>Gallery</Text>
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,

  imageWrapper: ViewStyle,
  image: ViewStyle,
};

const imageSize = (getWindowWidth() - 40) / 3;

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
    paddingLeft: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  imageWrapper: {
    width: imageSize,
    height: imageSize,
    marginRight: 10,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: imageSize,
    height: imageSize,
  },
});

export default connect(select, mapActions)(Gallery);
