import React from 'react';
import { Text, ViewStyle, StyleSheet } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { connect } from 'react-redux';
import BackgroundGradient from 'components/BackgroundGradient';

const select = ({ state }: any) => ({});
const mapActions = {};

type mappedProps = ReturnType<typeof select>;
type mappedActions = typeof mapActions;

type Props = {} & NavigationScreenProps<any> & mappedProps & mappedActions;
type State = {};

class Filters extends React.Component<Props, State> {
  render() {
    return (
      <BackgroundGradient fixed={ true } style={ styles.container }>
        <Text>Filters</Text>
      </BackgroundGradient>
    );
  }
}

type Styles = {
  container: ViewStyle,
};

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
  },
});

export default connect(select, mapActions)(Filters);
