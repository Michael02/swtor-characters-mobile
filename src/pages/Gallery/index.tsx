import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import * as ROUTES from 'constants/routes';
import Gallery from './Gallery';
import { get } from 'helpers/translations';
import { styles } from 'styles/header';
import NavigationService from 'services/navigation.service';
import Icon from 'components/Icon';
import { WHITE } from 'styles/global';
import Filters from './Filters';

const galleryNavigator = createStackNavigator(
  {
    [ROUTES.GALLERY]: {
      screen: Gallery,
      navigationOptions: {
        title: `${ get('label.gallery') }`,
      },
    },
    [ROUTES.GALLERY_FILTERS]: {
      screen: Filters,
      navigationOptions: {
        headerLeft: (
          <TouchableOpacity
            style={ styles.headerLeft }
            onPress={ () => NavigationService.goBack() }
          >
            <Icon
              name="arrow_left"
              size={ 20 }
              color={ WHITE }
              style={ {
                width: 20,
                height: 20,
              } }
            />
          </TouchableOpacity>
        ),
        headerRight: <View style={ styles.headerRight }/>,
      },
    },
  },
  {
    initialRouteName: ROUTES.GALLERY,
    navigationOptions: {
      gesturesEnabled: false,
      headerStyle: styles.header,
      headerTitleStyle: styles.headerTitle,
    },
  },
);

export default galleryNavigator;
