# swtor-characters-mobile
quick starter for react-native app with typescript and redux
```
react: 16.6.0-alpha.8af6728,
react-native: 0.57.4,
``` 

## Download
### With GIT
```
git clone https://gitlab.com/Michael02/react-native-typescript-starter.git
```
or (if you have [ssh key](https://help.github.com/enterprise/2.12/user/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/))
```
git clone git@gitlab.com:Michael02/react-native-typescript-starter.git
```
### Or just click download button 
on top of repository or [here](https://gitlab.com/Michael02/react-native-typescript-starter/-/archive/master/react-native-typescript-starter-master.zip) to download .zip

## Initialization
```
npm install
```
with yarn
```
yarn
```

## Startup
```
npm start
```
with yarn
```
yarn start
```

## Running
### IOS
```
npm run ios
```
with yarn
```
yarn run ios
```

### Android
you have to have running emulator or connected android device
```
npm run android
```
with yarn
```
yarn run android
```
